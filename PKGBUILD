# Maintainer: Mark Wagie <mark at manjaro dot org>
# Contributor: Bernhard Landauer <bernhard@manjaro.org>
# Contributor: Dan Johansen
# Contributor: BlackIkeEagle <ike DOT devolder AT gmail DOT com>
# Contributor: TZ86

pkgname=vivaldi
_rpmversion=7.1.3570.58-1
pkgver=7.1.3570.58
pkgrel=1
pkgdesc='An advanced browser made with the power user in mind.'
url="https://vivaldi.com"
options=(!strip !zipman)
license=('custom')
arch=('x86_64' 'aarch64')
depends=('libcups' 'nss' 'alsa-lib' 'libxss' 'ttf-font' 'desktop-file-utils' 'shared-mime-info' 'hicolor-icon-theme')
makedepends=('nodejs' 'w3m')
optdepends_x86_64=(
    'org.freedesktop.secrets: better secret storage in gnome-keyring or kwallet'
    'libnotify: native notifications'
    'vivaldi-ffmpeg-codecs: playback of proprietary video/audio'
    'gtk3: gtk3 integration'
    'qt5-base: qt5 integration'
    'qt6-base: qt6 integration'
)
optdepends_aarch64=(
    'org.freedesktop.secrets: better secret storage in gnome-keyring or kwallet'
    'libnotify: native notifications'
    'gtk3: gtk3 integration'
    'qt5-base: qt5 integration'
    'qt6-base: qt6 integration'
)
source=('0001-add-support-for-user-flags.patch'
        'vivaldi-bookmarks-merge.tar.gz')
source_x86_64=("https://downloads.vivaldi.com/stable/vivaldi-stable-${_rpmversion}.x86_64.rpm")
source_aarch64=("https://downloads.vivaldi.com/stable/vivaldi-stable-${_rpmversion}.aarch64.rpm")
sha512sums=('73745f1739ed638f8c7f61493d61a6c7f07dc30de7f23aeea79ffeaa75a2d9ec5a0b46f7be645ed4414294b4ce93960df808e85db4689c9b8b1ab4e63528ad49'
            '1fdc00cee6f223a2db82ea4b46bd597123559824a61c201eebb1bbe425a837ec9dd1ec159a2c0089fd4667d68b81e9bd322106b74f0a1ea3d35606fea9f3eafd')
sha512sums_x86_64=('7a8beca8227c2733bbe533cce747866649dca3c575b01595ec5f0124d10dfed60db1928776566f2dc6c51e419c39a5a7ac25db51dada38a01abf0394af2be25c')
sha512sums_aarch64=('e4d709ae00cf9ff52f1bbfc9918439923da6ffbd0f203a1a2d2f90a6d43e1a56d5a9c548b6ea319900287d968a1f11e3e0f7fdbe3fe33c3d5b45bdd03e8d02cf')

prepare() {
    mkdir -p vivaldi-bookmarks-merge/vivaldi-repack
    mv -t vivaldi-bookmarks-merge/vivaldi-repack etc opt usr
    cd vivaldi-bookmarks-merge

    # watch for additional localizations added in the future. Error messages will be displayed during merge.
    lang='be-BY de-AT de-CH de-DE dk-DK en en-AU en-CA en-GB en-IE en-IN en-NZ es-AR es-CL es-CO es-ES es-MX es-PE es-VE fr-BE fr-FR id-ID is-IS it-IT ja-JP ka-KA mk-SG my-MY nb-NO nl-BE nl-NL nn-NO ph-PH pl-PL pt-BR pt-PT ru-RU sv-SE tr-TR uk-UA vi-VI zh-HK zh-TW'

   for l in ${lang}; do
       cp manjaro/default-bookmarks/en-US.json manjaro/default-bookmarks/${l}.json
   done

    node merge.js manjaro vivaldi-repack/opt/vivaldi/resources/vivaldi
    cp -r vivaldi-repack/* ../
}

package() {
    cp --parents -a {opt,usr/bin,usr/share} "$pkgdir"

    # add support for ~/.config/vivaldi-stable.conf
    patch -p1 -i "$srcdir/0001-add-support-for-user-flags.patch" \
        "$pkgdir/opt/$pkgname/$pkgname"

    # remove "orig" leftover if it is there
    rm -f "$pkgdir/opt/$pkgname/$pkgname.orig"

    # suid sandbox
    chmod 4755 "$pkgdir/opt/$pkgname/vivaldi-sandbox"

    install -dm755 "$pkgdir/usr/bin"

    # make /usr/bin/vivaldi-stable available
    binf="$pkgdir/usr/bin/vivaldi-stable"
    if [[ ! -e "$binf" ]] && [[ ! -f "$binf" ]] && [[ ! -L "$binf" ]]; then
        ln -s /opt/vivaldi/vivaldi "$binf"
    fi
    # make /usr/bin/vivaldi available
    binf="$pkgdir/usr/bin/vivaldi"
    if [[ ! -e "$binf" ]] && [[ ! -f "$binf" ]] && [[ ! -L "$binf" ]]; then
        ln -s /opt/vivaldi/vivaldi "$binf"
    fi

    # install icons
    for res in 16 22 24 32 48 64 128 256; do
        install -Dm644 "$pkgdir/opt/$pkgname/product_logo_${res}.png" \
            "$pkgdir/usr/share/icons/hicolor/${res}x${res}/apps/$pkgname.png"
    done

    # install global icon in case hicolor theme gets bypassed
    install -Dm644 "$pkgdir/opt/$pkgname/product_logo_256.png" \
        "$pkgdir/usr/share/pixmaps/$pkgname.png"

    # license
    install -dm755 "$pkgdir/usr/share/licenses/$pkgname"
    w3m -dump "$pkgdir/opt/$pkgname/LICENSE.html" \
        | head -n 5 \
        > "$pkgdir/usr/share/licenses/$pkgname/license.txt"

    # https://archlinux.org/todo/legacy-path-for-metainfo-files/
    install -Dm644 "usr/share/appdata/$pkgname.appdata.xml" -t \
      "$pkgdir/usr/share/metainfo/"
    rm -rv "$pkgdir/usr/share/appdata"
}

